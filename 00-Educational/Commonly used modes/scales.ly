\version "2.16.2"

% {{{ Header
\header {
    tagline = ""
}
% }}}

% {{{ Paper and layout settings
\paper {
    staff-height = 6.8\mm
    between-system-space = 7.3\cm
    between-system-padding = #1
    max-systems-per-page = 13
    left-margin = 2\cm
    right-margin = 2\cm
    top-margin = 1\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}
% }}}

% {{{ Scales

ionian = {
  \mark "Ionian"
  c4 d e f g a b c
}

dorian = {
  \mark "Dorian"
  c4 d es f g a bes c
}

phrygian = {
  \mark "Phrygian"
  c4 des es f g as bes c
}

lydian = {
  \mark "Lydian"
  c4 d e fis g a b c
}

mixolydian = {
  \mark "Mixolydian"
  c4 d e f g a bes c
}

aeolian = {
  \mark "Aeolian"
  c4 d es f g as bes c
}

locrian = {
  \mark "Locrian"
  c4 des es f ges as bes c
}

melminor = {
  \mark "Melodic Minor"
  c4 d es f g a b c
}

dorianbnine = {
  \mark "Dorian b9"
  c4 des es f g a bes c
}

lydianaug = {
  \mark "Lydian Augmented"
  c4 d e fis gis a b c
}

lydiandom = {
  \mark "Lydian Dominant"
  c4 d e fis g a bes c
}

mixolydianbsix = {
  \mark "Mixolydian b6"
  c4 d e f g as bes c
}

locriantwo = {
  \mark "Locrian #2"
  c4 d es f ges as bes c
}

altered = {
  \mark "Altered"
  c4 des es fes ges as bes c
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { 
    title = "Major scale modes and derived chords"
  }
  << 
    \new ChordNames \chords {
      c1*2:maj7
      c:m7
      c:m7
      c:maj7.11+
      c:7
      c:m7
      c:m7.5-
    }
    \new Staff = one {
      \clef violin \key c \major
      \relative c' { \ionian \break }
      \relative c' { \dorian \break }
      \relative c' { \phrygian \break }
      \relative c' { \lydian \break }
      \relative c' { \mixolydian \break }
      \relative c' { \aeolian \break }
      \relative c' { \locrian \break }
    }
  >>

}

\bookpart {
  \header { 
    title = "Melodic minor scale modes and derived chords"
  }
  << 
    \new ChordNames \chords {
      c1*2:m7.7+
      c:m7.4.9-
      c:7.11+
      c:maj7.11+.5+
      c:7.13-
      c:m7.5-
      c:7.9+.5+
    }
    \new Staff = one {
      \clef violin \key c \major
      \relative c' { \melminor \break }
      \relative c' { \dorianbnine \break }
      \relative c' { \lydianaug \break }
      \relative c' { \lydiandom \break }
      \relative c' { \mixolydianbsix \break }
      \relative c' { \locriantwo \break }
      \relative c' { \altered \break }
    }
  >>

}

\bookpart {
  \header { 
    title = "Commonly used scales in the order of darkness"
  }
  << 
    \new ChordNames \chords {
      c1*2:maj7.11+
      c:maj7
      c:7
      c:m7
      c:m7
      c:m7
      c:m7.5-
    }
    \new Staff = one {
      \clef violin \key c \major
      \relative c' { \lydian \break }
      \relative c' { \ionian \break }
      \relative c' { \mixolydian \break }
      \relative c' { \dorian \break }
      \relative c' { \aeolian \break }
      \relative c' { \phrygian \break }
      \relative c' { \locrian \break }
    }
  >>
}

\bookpart {
  \header { 
    title = "Various scales for dominant chords"
  }
  << 
    \new ChordNames \chords {
      c1*2:7
      c:7.11+
      c:7.13-
      c:7.9+.5+
    }
    \new Staff = one {
      \clef violin \key c \major
      \relative c' { \mixolydian \break }
      \relative c' { \lydiandom \break }
      \relative c' { \mixolydianbsix \break }
      \relative c' { \altered \break }
    }
  >>
}

%}}}
