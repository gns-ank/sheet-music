------------------
 Sheet Music Repo
------------------

The purpose of this is to more easily share with my band mates the sheet music
I have typeset.

NOTE
----

This work should be exempt from Copyright restriction under the following
clause:

 Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is
 made for "fair use" for purposes such as criticism, comment, news reporting,
 teaching, scholarship, and research. Fair use is a use permitted by copyright
 statute that might otherwise be infringing. Non-profit, educational or
 personal use tips the balance in favor of fair use.

.. vim: tw=78
