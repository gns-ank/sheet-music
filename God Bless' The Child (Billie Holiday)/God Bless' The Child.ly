\version "2.16.2"

% {{{ Header
\header {
    title = "God Bless' The Child"
    composer = \markup \center-column { "Arthur Herzog Jr." "and Billie Holiday" }
    meter = "Ballad"
    tagline = ""
}
% }}}

% {{{ Paper and layout settings
\paper {
     staff-height = 7.3\mm
%     between-system-space = 7.3\cm
%     between-system-padding = #1
%     max-systems-per-page = 13
%     left-margin = 2\cm
%     right-margin = 2\cm
%     top-margin = 1\cm
%     bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
\layout {
  \context {
    \Score
    \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
  }
}

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \partial 4 es8 f 
  \mark "A"
  \repeat volta 2 {
    g4 g f es8 f | g4 g f bes8 c | des4 des c bes8 c | des4 bes g2 |
    g8 g4 f8 c2 | g'8 g4 f8 ces4. g'8 | bes bes4 g8 bes4 g4 |
    g8 bes bes2 bes,4 | c8 es es2. |
  }
  \alternative {
    { r2 r4 es8 f }
    { r1 \bar ".|" }
  }

  \mark "B"
  es'4 c es c | es  c8 es~ es2 | d4 bes d bes | d1 |
  es4 c es c | es  c8 es~ es2 | bes4 c c c | f,2. es8 f| \bar ".|"

  \mark "A"
  g4 g f es8 f | g4 g f bes8 c | des4 des c bes8 c | des4 bes g2 |
  g8 g4 f8 c2 | g'8 g4 f8 ces4. g'8 | bes bes4 g8 bes4 g4 |
  g8 bes bes2 bes,4 | c8 es es2. | r1 | \bar ".|"

  \break \mark "A"
  \repeat volta 2 {
    \comp #36
  }
  \alternative {
    { \comp #4 }
    { \comp #4 }
  }
  \mark "B"
  \comp #32 \bar ".|"
  \mark "A"
  \comp #40

}

Harmonies = \chords {
  \partial 4 s4
  \repeat unfold 2 {
    es4:maj7 es:7 as2:6 | es4:maj7 es:7 as2:6 | bes:m7 es:7 | bes:m7 es:7 |
    as:maj7 as:6 | as:m7.7+ as:m6 | g:m7 c:7.9- | f:m7 bes:7 |
    es1:6 | f2:m7 bes:7 | d:m7.5- g:7 |

    c2:m c:m7.7+ | c:m7 c:m6 | g1:m7 | d2:m7.5- g:7 |
    c2:m c:m7.7+ | c:m7 c:m6 | g:m7 c:7 | f:m7.5- bes:7 |

    es4:maj7 es:7 as2:6 | es4:maj7 es:7 as2:6 | bes:m7 es:7 | bes:m7 es:7 |
    as:maj7 as:6 | as:m7.7+ as:m6 | g:m7 c:7.9- | f:m7 bes:7 |
    es1:6 | \parenthesize { f2:m7 bes:7 }
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { instrument = "C edition" }
  \score { \transpose c f, {
  << 
    \new ChordNames \Harmonies
    \new Staff = one {
      \relative c'' {
        \clef violin \key es \major
        \melody
      }
    }
  >>
  } }
}

\bookpart {
  \header { instrument = "Eb edition" }

  \score { \transpose es f, {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key es \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bb edition" }

  \score { \transpose bes f {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key es \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bass edition" }
  \score { \transpose c f, {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef bass \key es \major
          \melody
        }
      }
    >>
  } }
}
% }}}
