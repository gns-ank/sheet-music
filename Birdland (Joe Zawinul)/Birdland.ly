\version "2.18.2"

\header {
  title = "Birdland"
  composer = "Joseph Zawinul"
  meter = "Med. straight 8ths"
}

upper = \relative c'' {
  \tempo 4=120
  \compressFullBarRests

  \key g \major
  \mark "Intro"
  
  \partial 8 r8 |
  \repeat volta 3 { r1 r1 r1 }
  \alternative {
    { r1 }
    {\ottava #1 r2 r4 r8 bes'~}
  }
  \break
  % Start drawing boxes around marks made with \mark
  \set Score.markFormatter = #format-mark-box-alphabet

\mark \default
  \repeat volta 4 {
    bes a bes a g4 e8 g~ | g4 e g a8 bes~ | bes4 a g e8 d
    \ottava #0
  } \alternative {
    {r8 g4.~ g4 r8 bes\laissezVibrer }
    {r8 g4.~ g4 r8 bes,~ }
  }
  \break
  
  \mark \default
  bes4 a8 a r4 r8 a~ | a4 g8 g8 r4 r8 a~ | a8 a fis fis r4 r8 a~ | 
  a4 g8 g8 r4 r8 a~  | a4 g8 g r4 r8 bes~ | bes4 a r2 | r8 g a bes b r g r |
  bes r a4 r2 | \repeat unfold 4 {r1}
  \break
  
  \override TextSpanner.bound-details.left.text = "G pedal"
  \mark \default
  \repeat volta 3 {
    b,4_"Melody played by piano or guitar"\startTextSpan r8 b r c d f~ | f4 d c r8 c | 
    r4 r8 c r c d f~ | f4 d c_"Play ×3" r8 b~\stopTextSpan |
  } 

  b4 r8 b r c d f~ | f4 d c r8 c | 
  r4 r8 c r c d f~ | f4 d c r8 b~ |

  b4 r8 b r c d f~ | f4 d c r8 c | 
  r4 r8 c r c d f~ | f4 d c r4 |
  \break
  
  \mark \default
  <<
    {d4. d8~ d4 r8 d | e4 d8 f~ f2 }
    {g4. g8~ g4 r8 g | g4 g8 b~ b2 }
    {b4. b8~ b4 r8 b | c4 b8 d~ d2 }
  >>
  <<
    { f,1 | r1}
    { b1 | r1}
    { d1 | r1}
    \\
    
    { b,4 r8 b r c d f~ | f4 d c r4 |}
  >>
  <<
    {d4. d8~ d4 r8 d | e4 d8 f r4 a~ | a1~ | a~ | a~ | a4 r r2 |}
    {g4. g8~ g4 r8 g | g4 g8 b r4 d~ | d1~ | d~ | d~ | d4 r r2 |}
    {b4. b8~ b4 r8 b | c4 b8 d r4 f~ | f1~ | f~ | f~ | f4 r r2 |}
  >>
  R1*7 | r2. r8 b,~ |
  \break
  
  
  \mark \default
  \repeat volta 2 {
    b4 d g, r8 g~ | g4 b g r8 d | d'2. r4 |
    c8 b4 a8~ a g4 b8~ | b4 d g, r8 a~ | a4 b g r8 d |
    d'1 | d8 e4 b8~ b g4 b8~ |
  }
  \break
  b4 d g r8 g,~ | g4 b e r8 d~ | d1 |
  c8 b4 a8~ a g4 b8~ | b4 d g r8 a,~ | a4 b g r8 d |
  d'1 | d8 e4 b8~ b g4. |
  \break
  
  \mark \default
  R1*7\startTextSpan  | r2 r4 d8(_"trmb or piano" e) |
  \repeat volta 3 {
    g4 a bes8( b d, e) | g g4 g4. bes8 g~ | g1~ | 
  } \alternative {
    { g4 d'8 bes~ bes4 d,8( e) e }
    { g4 d'8 bes~ bes4\stopTextSpan r8 f'~ }
  }
  \break
  
  \mark \default
  \repeat volta 6 {
    f4 e es d | des4 c b bes8_"Play ×6" f'~ |
  }
  f4 e es d | des4 c b bes8 g~ |
  g1 | R1*3 |
}

lower = \relative c {
  \clef bass
  \key g \major
  \time 4/4

  \partial 8 b8~
  \repeat volta 3 {
    b2 c4. d8~ | d2~ d4. b8~ | b2 c4 d8 g~ |
  }
  \alternative {
    {g2~ g4. b,8\laissezVibrer }
    {g'2~ g4. b,8~ }
  }
  
  \repeat volta 4 {
    b2 c4. d8~ | d2~ d4. b8~ | b2 c4 d8 g~ |
  }
  \alternative {
    {g2~ g4. b,8\laissezVibrer }
    {g'2. g8 g,8~ }
  }
  
  g4 r4 r8 a4 bes8~ | bes4 r4 r8 bes8 c  d~ | d4 r8 g, r a4 c8~ | c2 r8 a8 as b~ | 
  b d e g r4 r8 c, | r d es4 r2 | r8 g,4 a8 r bes r b | r c4 r8 r4 r8 g~ | g1~ | g1~ | g1~ | g1 |

  \repeat volta 3 {
    r1 r1 r1 r2 r4 r8 b~_"Tacet first two times"
  }
  
  b1~ b2. r8 c~ | c1~ c2. r8 d~ | d1~ d2. r8 e~ | e4.\glissando c8~ c2~ | c1 |
  
}

Harmonies = \chords {
  \partial 8 s8 | r1*5 |
  s1*4 | s2. s8 g8:m7~ | g4:m7 f4:/g s4. f8:/bes~ | s4 es4:/bes s4. d8:m7 | s2. s8 f8/c | 
  s4 c4:m7 s4 s8 f8/b | s4 e4:m7 s4. g8:m7/c | s4 f4:/es s2 | s8 e:m7 f ges g s e:m7 s | g:m7/c s8 f4:/c s2 | g1|
  s1*3
  s1*4
  s1*8
  
  g1 | c4. g2 s8 | s1*2
  g1 | c4. g4. b4:m7.5- | s1*4
  g1*8:7
  
  g4 b:m7 e:m7 s8 g8:/b | s4 c:maj7 cis:m7.5- s |
  b2:m7 e:7 | a8:m7 g4:/b c:6 c/d g8 |
  s4 b:m7 e:m7 s8 c:6 | 
  s4 cis:m7.5- c/d s4 | c1:maj7 | s8 a4:m7 c:maj7/e a:m7/d g8 |
  
  g4 b:m7 e:m7 s8 g8:/d | s4 cis:m7.5- c:9 s |
  b2:m7 e:7 | a8:m7 g4:/b c:6 c/d g8 |
  s4 b:m7 e:m7 s8 a8:m7/d | 
  s4 cis:m7.5- c:9 s8 b:m7 | s2 e2:7 | a8:m7 g4:/b c:6 c4.:/d |
  
  \repeat unfold 13 { s1 }
  
  g4:9 ges:9 f:9 e:9 | es:9 d:9 des:9 c8:9 g:9 |
  s4 ges:9 f:9 e:9 | es:9 d:9 des:9 c8:9 g | s1
}

Sometext = \markup {
  \fill-line{
    \hspace #3
    \column{
      \line{D.S. to \bold{A} (leave the bass alone on the first 2 repeats),} 
      \line{take repeats, play through \bold{C} , go to \bold{E}.}
      \line{Play solos, repeat and fade out.}
      \line{During last two bars of \bold{C} played the second time, bass plays last two bars of \bold{D}.}
    }
  }
}

\bookpart {
  \header { instrument = "C edition" }
  \score {
    <<
      \new ChordNames \Harmonies
      \new PianoStaff <<
        \set PianoStaff.instrumentName = #"Piano  "
        \new Staff = "upper" \upper
        \new Staff = "lower" \lower
      >>
    >>
  }
  
  \Sometext
}

\bookpart {
  \header { instrument = "Eb edition" }
  \score {
  \transpose es c' {
  <<
      \new ChordNames \Harmonies
      \new PianoStaff <<
        \set PianoStaff.instrumentName = #"Piano  "
        \new Staff = "upper" \upper
        \new Staff = "lower" \lower
      >>
    >>
  }
  }
  
  \Sometext
}

\bookpart {
  \header { instrument = "Bb edition" }
  \score {
    \transpose bes c' {
    <<
      \new ChordNames \Harmonies
      \new PianoStaff <<
        \set PianoStaff.instrumentName = #"Piano  "
        \new Staff = "upper" \upper
        \new Staff = "lower" \lower
      >>
    >>
  }
  }
  
  \Sometext
}
