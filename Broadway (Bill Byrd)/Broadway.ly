\version "2.16.2"

% {{{ Header
\header {
    title = "Broadway"
    composer = "Bill Byrd/Teddy McRae/Henry Woode"
    meter = "Swing"
}

% mes major = es \major
% }}}

% {{{
\paper {
    staff-height = 7\mm
    between-system-space = 7.5\cm
    between-system-padding = #1
    max-systems-per-page = 10
    left-margin = 2.5\cm
    right-margin = 2.5\cm
    top-margin = 2.0\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
% \layout {
%   \context {
%     \Score
%     \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
%   }
% }

% Chord layouts and macros

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \repeat volta 2 {
    f8 es r4 r f8 es | r2 f8 es f es | f es r4 r f8 es | 
    r2 f8 es f es | g bes4.~ bes4 g8 bes,8~ | bes2 ges'8 es f ges |
  }

  \alternative{
    { f8 es4. r2 | r1 | }
    { f8 es4. r2 | r1 | }
  }
  \break

  bes'8 c4 es,8~ es4 bes'8 c~ | c es,4. bes'8 c4. |
  bes8 as bes as bes as4. | r1 |
  as8 bes4 des,8~ des4 as'8 bes~ | bes des,4. as'8 bes4. |
  as8 ges as ges as ges4. | g8 f g f g f4. |

  \break
  f8 es r4 r f8 es | r2 f8 es f es | f es r4 r f8 es | 
  r2 f8 es f es | g bes4.~ bes4 g8 bes,8~ | bes2 ges'8 es f ges |
  f8 es4. r2 | r1 |

  \break
  \mark "Solos"
  \repeat volta 2 {
    \comp #24
  }

  \alternative{
    { \comp #8 }
    { \comp #8 }
  }
  \break
  \comp #32

  \break
  \comp #32
}

Harmonies = \chords { 
  \repeat unfold 2 {
    es1:6 | s | as:7 | s | f:m7 bes:7 |
    es:6 | f2:m7 bes:7 | es1:6 | s |
    bes:m7 | es:7 | as:maj7 | s |
    as:m7 | des:7 | ges:maj7 | f2:m7 bes:7 |
    es1:6 | s | as:7 | s | f:m7 | bes:7 | es:6 | s |
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { subtitle = "C edition" }
  \score { \transpose c c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key es \major
          \melody
        }
      }
     >>
  } }
}

\bookpart {
  \header { subtitle = "Eb edition" }

  \score { \transpose es, c, {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key es \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bb edition" }

  \score { \transpose bes, c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key es \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bass edition" }

  \score { \transpose c c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c, {
          \clef bass \key es \major
          \melody
        }
      }
    >>
  } }
}
% }}}
