\version "2.16.2"

% {{{ Header
\header {
    title = "Nuages"
    composer = "Django Reinhardt/ Jacques Larue"
    meter = "Med. Slow"
}

% mes major = es \major
% }}}

% {{{
\paper {
    staff-height = 7\mm
    between-system-space = 7.5\cm
    between-system-padding = #1
    max-systems-per-page = 11
    left-margin = 2\cm
    right-margin = 2\cm
    top-margin = 1\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
% \layout {
%   \context {
%     \Score
%     \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
%   }
% }

% Chord layouts and macros

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \partial 2. cis8 d a' as g fis \bar ".|"

  f2~ f4. e8 | es2~ es4. d8 | d1~ | d4 cis8 d a' as g fis |
  f2~ f4. e8 | es2~ es4. d8 | d1~ | d4 ais8 b b' a g fis |
  a1~ | a4 ais,8 b b' a g f | g1~ | g4 fis8 g b g \times 2/3 {e d cis~} |
  cis2 c | cis8 r e fis g a b d~ | d1~ | d4 cis,8 d a' as g fis |
  f2~ f4. e8 | es2~ es4. d8 | d1~ | d4 cis8 d d' cis c b |
  bes2. bes8 a | as2~ as4. g8 | g g4.~ g2~ | g4 fis8 g g' f es f |
  g1~ | g4 f,8 g g' f es f g1~ | g4 cis,,8 d a' as g fis |
  f2.~ \times 2/3 {f8 f e} | es2 d8 b'4. | g1~ | g2 r \bar "|."

  \break
  
  \comp #128
  \bar "|."
}

Harmonies = \chords {
  \partial 2. s2. |
  \repeat unfold 2 {
    bes2:m7 es:7 | a:m7.5- d:7.9- | g2:6 a4:m7 ais4:m7 | b1:m7
    bes2:m7 es:7 | a:m7.5- d:7.9- | g1*2:6 |
    fis1:m7.5- | b:7 | e1*2:m7 |
    a2:7 as:7 | a1:7 | d2.:7 a4:m7 | d1:7 |
    bes2:m7 es:7 | a:m7.5- d:7.9- | g1*2:6 |
    es2:m7 a:7 | d:m7.5- g:7.9- | c1*2:6 |
    c1:m7 | f:13 | g2:6  a4:m7 ais4:m7 | b1:m7 |
    bes2:m7 es:7 | a:m7.5- d:7.9- | g:6 c:9 | g1:6
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { subtitle = "C edition" }
  \score { \transpose c c {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key g \major
          \melody
        }
      }
     >>
  } }
}

\bookpart {
  \header { subtitle = "Eb edition" }

  \score { \transpose es c, {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key g \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bb edition" }

  \score { \transpose bes, c {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key g \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bass edition" }

  \score { \transpose c c' {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c, {
          \clef bass \key g \major
          \melody
        }
      }
    >>
  } }
}
% }}}
