\version "2.16.2"

% {{{ Header
\header {
    title = "Stompin' at the Savoy"
    composer = "Benny Goodman, Edgar Sampson and Chick Webb"
    meter = "Medium"
}
% }}}

% {{{ Paper and layout settings
\paper {
    staff-height = 7\mm
    between-system-space = 7.5\cm
    between-system-padding = #1
    max-systems-per-page = 11
    left-margin = 2\cm
    right-margin = 2\cm
    top-margin = 2\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
% \layout {
%   \context {
%     \Score
%     \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
%   }
% }

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music
melody = {
  \partial 2 f4. aes8~ |
  \repeat volta 2 {
    aes8 aes, bes f' c4 aes8 bes~ | bes2 f'4. aes8~ |
    aes8 aes, bes f' c4 aes8 bes~ | bes2 f'4. aes8~ |
    aes8 aes, bes des ges4 des8 ces~| ces2 f4. des8~ |
  }
  \alternative {
    { des1 | r2 f4. aes8~ | }
    { des,1 | r1 |}
  }
  \break

  aes'8 ges4. a8 g4. | aes8 ges4.~ ges4 r4 |
  gis8 fis4. b8 a4. | gis8 fis4.~ fis4 r4 |
  fis8 e4. g8 f4. | fis8 e4.~ e4 e4 |
  fis8 a a a~ a2 | f8 aes aes aes~ aes4 f8 aes~ |
  \break

  aes8 aes, bes f' c4 aes8 bes~ | bes2 f'4. aes8~ |
  aes8 aes, bes f' c4 aes8 bes~ | bes2 f'4. aes8~ |
  aes8 aes, bes des ges4 des8 ces~| ces2 f4. des8~ |
  des1 | r2 f4. aes8~ |
  \break

  \mark "Solos"

  \repeat volta 2 {
    \comp #12
  }
  \alternative {
    { \comp #8 }
    { \comp #8 }
  }

  \comp #76
}

Harmonies = \chords {
  \partial 2 aes2:7 |
  \repeat unfold 2 {
    des2*3:maj7 aes2:7 |
    des2*3:maj7 d2:dim7 |
    es1:m7 | aes:7 |
    des2:6 bes:m7 | es2:m7 aes:7 |
    des1:6 | des:7 |
    ges2:7 g:7 | ges1:7 |
    b2:7 fis:m7-5 | b1:7 |
    e2:7 f:7 | e1:7 |
    a:7 | aes:7 |
    des2*3:maj7 aes2:7 |
    des2*3:maj7 d2:dim7 |
    es1:m7 | aes:7 |
    des2*3:6 aes2:7
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { subtitle = "C edition" }
  \score { \transpose c c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key des \major
          \melody
        }
      }
     >>
  } }
}

\bookpart {
  \header { subtitle = "Bb edition" }

  \score { \transpose bes, c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef violin \key des \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Eb edition" }

  \score { \transpose es, c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef violin \key des \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bass clef edition" }

  \score { \transpose c c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c,, {
          \clef bass \key des \major
          \melody
        }
      }
    >>
  } }
}
% }}}
