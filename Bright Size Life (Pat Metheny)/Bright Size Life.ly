\version "2.16.2"

% {{{ Header
\header {
    title = "Bright Size Life"
    composer = "Pat Metheny"
    meter = "Straight 8ths/166"
}

% mes major = es \major
% }}}

% {{{
\paper {
    staff-height = 7\mm
    between-system-space = 7.5\cm
    between-system-padding = #1
    max-systems-per-page = 10
    left-margin = 2.5\cm
    right-margin = 2.5\cm
    top-margin = 2.0\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
% \layout {
%   \context {
%     \Score
%     \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
%   }
% }

% Chord layouts and macros

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  fis8 d' b fis' e b' a d, |
  \mark A
  \repeat volta 2 {
    e1 | b'4~ \times 2/3 { b8 a g } \times 2/3 { fis g a } \times 2/3 { fis g a} |
    e1~  | e4. g \times 2/3 {r8 fis e} |
    e16 fis d8~ d2.~ | d1 |
  }
  \alternative {
    {
      << f,8 d' >> << g, e' >> r << a, f' >> r << g, e' >> << f,4 d' >>
      fis,8 d' b fis' e b' a d, |
    }
    {
      << d8 b' >> << cis, a' >> r << b, g' >> r << cis, a' >> << a,4~ fis'~ >> |
      << a,1 fis' >> |
    }
  }
  \break

  \mark B
  r4 r8 cis' d, fis g, e''~ | e d cis4 r2 |
  e4~ \times 2/3 { e8 d c } b4 d8 a~ | a1 |
  r2 bis8 cis, fis g, | cis'4 b8 a r g4. |
  fis8 a r e'~ e d4. |
  fis,,8 d' b fis' e b' a d, |
  \break

  \mark C
  e1 | b'4~ \times 2/3 { b8 a g } \times 2/3 { fis g a } \times 2/3 { fis g a} |
  e1~  | e4. g \times 2/3 {r8 fis e} |
  e16 fis d8~ d2.~ | d1 |
  << cis4. e >> b8 a4. g8 | f4\staccato << cis'2. d >> |
  \break

  \mark A
  \repeat volta 2 {
    \comp #24
  }
  \alternative {
    { \comp #8 }
    { \comp #8 }
  }
  \break

  \mark B
  \comp #32
  \break

  \mark C
  \comp #32
}

Harmonies = \chords {
  r1 |
  \repeat unfold 2 {
    g1*2:maj7 | bes1*2:maj7.11+/a | d1 | d/c | bes:maj7 | r | g/b | d |
    g1*2/a | f/g | a:7/e | d1 | r |
    g1*2:maj7 | bes1*2:maj7.11+/a | d1 | d/c | a:7 | d:maj7
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { subtitle = "C edition" }
  \score { \transpose c c {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key d \major
          \melody
        }
      }
     >>
  } }
}

\bookpart {
  \header { subtitle = "Eb edition" }

  \score { \transpose es c, {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key d \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bb edition" }

  \score { \transpose bes, c {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key d \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bass edition" }

  \score { \transpose c c {
    <<
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c, {
          \clef bass \key d \major
          \melody
        }
      }
    >>
  } }
}
% }}}
