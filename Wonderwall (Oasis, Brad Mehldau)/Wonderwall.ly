\version "2.18.2"

\header {
  title = "Wonderwal"
  composer = "Oasis (Brad Mehldau cover)"
  meter = "Med. straight 8ths"
}

upper = \relative c {
}

lower = \relative c, {
  \clef bass
  \key g \major
  \partial 8 e8~ |
  \repeat unfold 7 { 
    e8 e4. r16 g'8 e16~ e8 b d16 e e d e8 a,16 a bes b d e,~
  }
}

Harmonies = \chords {
 
}


\bookpart {
  \header { instrument = "C edition" }
  \score {
    <<
      \new ChordNames \Harmonies
      \new PianoStaff <<
        \set PianoStaff.instrumentName = #"Piano  "
        \new Staff = "upper" \upper
        \new Staff = "lower" \lower
      >>
    >>
  }
}
