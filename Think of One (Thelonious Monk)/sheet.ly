\version "2.16.20"

% {{{ Header
\header {
    title = "Think of One"
    composer = "Thelonious Monk"
    meter = "Blues"
}

% mes major = es \major
% }}}

% {{{
\paper {
    staff-height = 9\mm
    between-system-space = 7.5\cm
    between-system-padding = #1
    max-systems-per-page = 10
    left-margin = 2.5\cm
    right-margin = 2.5\cm
    top-margin = 2.0\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
\layout {
  \context {
    \Score
    \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
  }
}

% Chord layouts and macros

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music


bass_intro = {
  f8 e d c bes a g f |
}

other_intro = {
  r2. r8 c |
}

melody = {
  \repeat volta 2 {
    f8 c r f~ f2 |
    f4 f c c |
    f8 f f f f4 c8 es8~ |
    es2. r8 c |
    f c r f f2 |
    f4 f c c |
    f16 f f f b2. |
  }
  \alternative {
    { r2. r8 c, | }
    { r1 | }
  }
  \break

  f16 f f f f2 c8 es |
  r es4.~ es4. d8 |
  f8 f f f f4 b,8 as'~ |
  as2. a,8 f'~ |
  f4 bes,8 b~ b4 c8 des |
  r des4.~ des4. d8 |

  << { f8 f f f f4 e8 ges~ } { des8 c ces b a4 aes8 des~ } >> |
  << ges2. des2. >> r8 \parenthesize c \bar "|."
  \break

  \mark "Solos"
  \repeat volta 2 {
    \comp #28
  }
  \alternative {
    { \comp #4 }
    { \comp #4 }
  }
  \break

  \comp #32
}

Harmonies = \chords {
  s1
  \repeat unfold 2 {
    f4. b2:7.11+ s8 | d2:m7 es:13 | as:7.13 des:7.13 | ges1:7.13.5- |
    f4. b2:7.11+ s8 | d2:m7 es:13 | as:7.13 des:7 |
    ges1:13 | ges:13 |
    f | c2:m7 f2:7 | bes1:maj7 | g:7.9- |
    d:m7 | g:7.5- | g:dim7 | c:7.5-.9-
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { subtitle = "C edition" }
  \score { \transpose c c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key f \major
          \other_intro
          \melody
        }
      }
     >>
  } }
}

\bookpart {
  \header { subtitle = "Eb edition" }

  \score { \transpose es, c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key f \major
          \other_intro
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bb edition" }

  \score { \transpose bes, c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key f \major
          \other_intro
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bass edition" }

  \score { \transpose c c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c, {
          \clef bass \key f \major
          \bass_intro
          \melody
        }
      }
    >>
  } }
}
% }}}
