\version "2.16.2"

% {{{ Header
\header {
    title = "Summertime"
    composer = \markup \center-column { "George Gershwin" }
    meter = "Ballad"
    tagline = ""
}
% }}}

% {{{ Paper and layout settings
\paper {
    staff-height = 6.8\mm
    between-system-space = 7.3\cm
    between-system-padding = #1
    max-systems-per-page = 13
    left-margin = 2\cm
    right-margin = 2\cm
    top-margin = 1\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
\layout {
  \context {
    \Score
    \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
  }
}

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \partial 4 g8 es 
  \repeat volta 2 {
    g1~ | g8 r f8. es16 f8. g16 es4 | c2 g2~ | g4 r g' es |
    f8 f4.~ f2 | r4 es8. c16 es8. c16 es4 | d1~ | d2 r8 g4 es8 |
    g g~ g2. | r4 f8. es16 f8. g16 es4 | c2 g2~ | g2 r4 g |
    bes g8 bes c es~ es4 | g8 f4. es2 | c1~ | c4 r r g'8 es |
  }

  \break \mark "Solos"
  \comp #64
}

Harmonies = \chords {
  \partial 4 s4
  \repeat unfold 2 {
    c2:m6 d:m6 | c:m6 d:m6 | c:m6 d:m6 | c:m6 c:7.9+ |
    f2:m7 g:m7 | f:m7 as:7 | g:7 d:m7.5- | g1:7.5+.9+ |
    c2:m6 d:m6 | c:m6 d:m6 | c:m6 d:m6 | c:m6 f4:m7 bes:7 |
    es2:maj7 as:7 |  d:7.9+ g:7.9+ | c:m6 d:m6 | c:m6 g:7.9+ |
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { instrument = "C edition" }
  << 
    \new ChordNames \Harmonies
    \new Staff = one {
      \relative c'' {
        \clef violin \key es \major
        \melody
      }
    }
  >>

}

\bookpart {
  \header { instrument = "Eb edition" }

  \score { \transpose es c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key es \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bb edition" }

  \score { \transpose bes c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key es \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bass edition" }

    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef bass \key es \major
          \melody
        }
      }
    >>
}
% }}}
