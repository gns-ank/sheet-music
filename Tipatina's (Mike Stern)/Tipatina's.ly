\version "2.18.2"

\header {
  title = "Tipatina's"
  composer = "Mike Stern"
  meter = "110 BPM, Funk"
}

guitar = \relative c''' {
  \key g \major
  r1 |
  \mark "A1"
  \repeat volta 4 {
    r16 g r fis r e r e fis8 g16 r r g r fis |
  }
  \alternative {
    {r e r e c8 a r2 |}
    {e'16 r e e c8 a r4 r16 fis' r e|}
  }
  
  \break \mark "B"
  es c a fis c'8 a16 c d8 c16 e, r16 e'16 es e |
  es c a fis c'8 a16 d8. r16 fis16 r16 e es e |
  es c a fis c'8 a16 c es8 d16 c16~ c a g e |
  es e g e c' a8 e16 g16 r g16 r r16 fis' r e |
  es c a fis c'8 a16 c d8 c16 es r8 r32 fis32( f e | 
  es16) c a fis c'16 a es' d8. r16 fis r e es e |
  es c a fis c'8 a16 c es8 d16 c16~ c a g e |
  \repeat volta 2 {
    es16 e g e c' a8. d c16 d c a8 |
    es16 e g e c' a8 e16 g16 r g16 r r8 g16 r |
  }

  \break \mark "A1"
  a8. a16 r8 g16 e8. g8 e g |
  a8. a16 r8 g16 e8. r8 r4 |
  a8. a16 r8 g16 e8. g8 e g |
  a8. a16 r8 g16 e8. r8 r8 fis'16 e |

  \break \mark "B"
  es c a fis c'8 a16 c d8 c16 e, r16 e'16 es e |
  es c a fis c'8 a16 d8. r16 fis16 r16 e es e |
  es c a fis c'8 a16 c es8 d16 c16~ c a g e |
  es e g e c' a8 e16 g16 r g16 r r16 fis' r e |
  es c a fis c'8 a16 c d8 c16 es r8 r32 fis32( f e | 
  es16) c a fis c'16 a es' d8. r16 fis r e es e |
  es c a fis c'8 a16 c es8 d16 c16~ c a g e |
  \repeat volta 2 {
    es16 e g e c' a8. d c16 d c a8 |
    es16 e g e c' a8 e16 g16 r g16 r r8 g16 r |
  }
  
  \break \mark "A2"
  \repeat volta 2 {
    r16 c' r b r a r a b8 c16 r r c r b |
    r a r a f8 c r2 |
    r16 c' r b r a r a b8 c b a |
  }
  
  \alternative {
    {g8. f16 as8. f16 g16 f d8  c16 d r d |}
    {
      r16 a' r a f'4 d c8 c | 
      c c c r r2 |
    }
  }
  
  \break \mark "Ending riff"
  es,,16 e g e c' a8. d c16 d c a8 |
  es16 e g e c' a8 e16 g16 r g16 r r8 g16 r |
  es16 e g e c' a8. d c16 d c a8 |
  r16 fis es e es c a fis es' d c a g e g g |
  r8 a8~ a2. |
}


Harmonies = \chords {
   
}


\bookpart {
  \header { instrument = "C edition" }
  \score {
    <<
      \new ChordNames \Harmonies
      \new Staff = "Guitar" \guitar
    >>
  }
}
