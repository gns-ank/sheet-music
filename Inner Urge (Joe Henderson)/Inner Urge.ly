\version "2.16.2"

% {{{ Header
\header {
    title = "Inner Urge"
    composer = \markup \center-column { "Joe Henderson" }
    meter = "Mid-Fast"
    tagline = ""
}
% }}}

% {{{ Paper and layout settings
\paper {
    staff-height = 6.8\mm
    between-system-space = 7.3\cm
    between-system-padding = #1
    max-systems-per-page = 13
    left-margin = 2\cm
    right-margin = 2\cm
    top-margin = 1\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \repeat volta 2 {
    r4. fis8 b c fis, b |
    c4. fis,8 b c r4 |
    r4. fis,8 b c fis, b |
    c fis, b c r2 |
    \break

    r4. f,8 b c f, b |
    c4. f,8 b c r4 |
    r4. f,8 b c f, b |
    c f, b c r2 |
    \break

    r4. bes8 a es a, d |
    es4. a,8 cis d r4 |
    r4. bes'8 a es a, d |
    es a, cis d~ d f4 g8~ |
    \break

    g4. g8 f c g' c |
    des4. f8 g c4. |
    r4. g,8 f c g' c |
    des f g c r bes4 dis8~ |
    \break

    dis4. cis8 b gis fis e |
    es d des es c e \times 2/3 {f as bes} |
    cis4. b8 a4 fis8 ais~ |
    ais8 f4. r4 fis4 |
    \break

    b4~ b16 g e c b4~ \times 4/5 { b16 c e g b } |
    gis4~ gis16 e cis gis fis4~ \times 4/5 { fis16 a cis e gis } |
    g4~ g16 f c g f4~ \times 4/5 { f16 g c f g } |
    a4. fis8 a fis r4 \bar "'."
    \break
  }

  \break \mark "Solos"
  \comp #96
}

Harmonies = \chords {
  \repeat unfold 2 {
    fis1*4:m7.5-

    f:maj7.11+

    es:maj7.11+

    des:maj7.11+

    e1:maj7.11+
    des:maj7.11+
    d:maj7.11+
    b:maj7.11+

    c:maj7
    a:maj7
    bes:7
    g:maj7
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { instrument = "C edition" }
  << 
    \new ChordNames \Harmonies
    \new Staff = one {
      \relative c' {
        \clef violin \key g \major
        \melody
      }
    }
  >>

}

\bookpart {
  \header { instrument = "Eb edition" }

  \score { \transpose es c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key g \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bb edition" }

  \score { \transpose bes c'' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef violin \key g \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bass edition" }

    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c, {
          \clef bass \key g \major
          \melody
        }
      }
    >>
}
