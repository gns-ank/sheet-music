\version "2.16.20"

% {{{ Header
\header {
    title = "Cute"
    composer = "Neil Hefti"
    meter = "Swing"
}
% }}}

% {{{
\paper {
    staff-height = 7\mm
    between-system-space = 7.5\cm
    between-system-padding = #1
    max-systems-per-page = 10
    left-margin = 3\cm
    right-margin = 3\cm
    top-margin = 2.5\cm
    bottom-margin = 4\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
\layout {
  \context {
    \Score
    \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
  }
}

% Chord layouts and macros

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \repeat volta 2 {
    r8 d4. e4 f | g2 a4. e8~ | e4 r4 r2 | r1 | \break
    r8 d4. e4 f | g2 a4. g8~ | g4 r4 r2 | r1 | \break
    r8 a4. b4 c8 d8~ | d2 r2 | r8 g,4. a4 b8 c8 ~| c2 r2 | \break
  }

  \alternative{
    { r8 fis,4. g4 a | b2 fis4. b8~ | b2 r2 | r1 \break}
    { r8 f4. g4 a4 | c2 d4. c8 | r1 | r1 }
  }

  \mark "Solos"
  \repeat volta 2 {
    \comp #48
  }

  \alternative{
    { \comp #16 }
    { \comp #16 }
  }
}

Harmonies = \chords { 
  \repeat unfold 2 {
    d1:m7 g:7 c:maj7 a:7
    d:m7 g:7 g:m7 c:7
    f:maj7 f:m6 c2 b4:m7.5- e4:7 a1:m7
    fis1:m7 b:7 e:maj7 a:7
    d:m7 g2:7sus s4. c8:6 \skip1 \parenthesize e2:m7 a:7
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { subtitle = "C edition" }
  \score { \transpose c c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key c \major
          \melody
        }
      }
     >>
  } }
}

\bookpart {
  \header { subtitle = "Eb edition" }

  \score { \transpose es, c, {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key c \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bb edition" }

  \score { \transpose bes, c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key c \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bass edition" }

  \score { \transpose c c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c, {
          \clef bass \key c \major
          \melody
        }
      }
    >>
  } }
}
% }}}
