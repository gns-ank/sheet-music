\version "2.16.2"

% {{{ Header
\header {
    title = "Watermelon Man"
    composer = "Herbie Hancock"
    meter = "Funk, straight 8ths"
}

% }}}
% {{{
\paper {
    staff-height = 7\mm
    between-system-space = 7.5\cm
    between-system-padding = #1
    max-systems-per-page = 10
    left-margin = 2.5\cm
    right-margin = 2.5\cm
    top-margin = 2.0\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
% \layout {
%   \context {
%     \Score
%     \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
%   }
% }

% Chord layouts and macros

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  % The whistles
  \mark "Whistle #1"
  \repeat volta 16 { 
    f8 des'16 r r f,16 des' g,~ g8 des'16 r g,8 des'16 r |
    f,8 des'16 r r f,16 des' g,~ g8 des'16 r g,8 des'16 r |
  } \break
  \mark "Whistle #2"
  r2 r4 r8 r16 f, | a a r f a a r2 r16 f |
  \repeat volta 7 { 
    a a r f a a r8 r16 \times 2/3 { f a a } f a a r f 
  } \break
  \mark "Whistle #3"
  | r2 r4 r8 des~ | \repeat volta 2 {
    des8 des4 r des des8~ | des8 r4 des des r8 | r8 des4 des r des8~ |
  } \break

  % The main bit
  \mark "Head"
  r4 c8 c g'4~ g16 as r e | r2 as16 g f d f d r8 |
  r4 c8 c g'4~ g16 as r e | r2 as16 g as g f d r8 |
  r4 c8 c g'4~ g16 as r e | r1 |
  \repeat volta 2 {
    r1 | r2 c8 des16 es c8 as16 ges~ | ges4. as4 r8 r4
  }
  \alternative {
    {r1}
    { r2 r16 as r g << ges4 es''4~ >> }
  }
  | es1~ | 
  es2 es16 d c bes c d r es,~ | es1~ | es2 r2|
  r2. r8 f'8~ | f1~ | f2 es16 d c bes c d r es,~ | es1~ | es2 r 2|
}

bass_riff = {
  \repeat volta 2 { r1 | r1 }
  r1 r1 \repeat volta 2 { r1 }
  r1 \repeat volta 2 { r1 }
  \break
  f8 r4 << es'8 g'8 >> r16 << e,8 gis'8 >> r16 << f,8 a'8 >> r |
  f,,8 f'8 r8 << es8 g'8 >> r16 << e,8 gis'8 >> r16 << f,8 a'8 >> r |
  f,,8 r4 << es'8 g'8 >> r16 << e,8 gis'8 >> r16 << f,8 a'8 >> r |
  f,,8 f'8 r8 << es8 g'8 >> r16 << e,8 gis'8 >> r16 << f,8 a'8 >> r |
  bes,,8 r4 << as,8 c'8 >> r16 << a,8 cis'8 >> r16 << bes,8 d'8 >> r |
  bes,8 bes' r << as,8 c'8 >> r16 << a,8 cis'8 >> r16 << bes,8 d'8 >> r |
  f,,8 r4 << es'8 g'8 >> r16 << e,8 gis'8 >> r16 << f,8 a'8 >> r |
  f,,8 f'8 r8 << es8 g'8 >> r16 << e,8 gis'8 >> r16 << f,8 a'8 >> r |
}

Harmonies = \chords { 
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { subtitle = "C edition" }
  \score { \transpose c c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key f \major
          \melody
        }
      }
     >>
  } }
}

\bookpart {
  \header { subtitle = "Eb edition" }

  \score { \transpose es, c, {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key f \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bb edition" }

  \score { \transpose bes, c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c' {
          \clef violin \key f \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { subtitle = "Bass edition" }

  \score { \transpose c c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c,, {
          \clef bass \key f \major
          \bass_riff
        }
      }
    >>
  } }
}
% }}}
