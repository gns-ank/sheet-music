\version "2.16.2"

% {{{ Header
\header {
    title = "'Round Midnight"
    composer = \markup \center-column { "Thelonious Monk, Cootie Williams" "and Bernie Hanighen" }
    meter = "Ballad"
    tagline = ""
}
% }}}

% {{{ Paper and layout settings
\paper {
     staff-height = 9.3\mm
%     between-system-space = 9.3\cm
%     between-system-padding = #1
%     max-systems-per-page = 10
%     left-margin = 2\cm
%     right-margin = 2\cm
%     top-margin = 1\cm
%     bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
\layout {
  \context {
    \Score
    \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
  }
  \context {
    \Staff
    \override VerticalAxisGroup #'default-staff-staff-spacing
         #'staff-staff-spacing = #2
  }
}

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \mark "A"
  \repeat volta 2 {
    r4 bes16 es f bes ges4. bes,8 | es8 es4. bes'4 as | r4 es16 ges bes des c4. es,8 |
    a4 as as g | r4 as16 ces es ges f4. ces8 | bes2. r4 |
  }
  \alternative {
    { ges4 f2~ f8 es | des bes'~ bes2. | }
    { ges8 f4. es es8~ | es1 | }
  }
  \break

  \mark "B"
  ges4 f2~ f8 es | des4 bes2. | ges'4 f2~ f8 es | des4 bes'2. |
  ces2 bes | ges f | es' des | ces bes |
  \break

  \mark "C"
  r4 bes,16 es f bes ges4. bes,8 | es8 es4. bes'4 as | r4 es16 ges bes des c4. es,8 |
  a4 as as g | r4 as16 ces es ges f4. ces8 | bes2. r4 |
  ges8 f4. es es8~ | es1 |

  % \break \mark "A"
  % \repeat volta 2 {
  %   \comp #24
  % }
  % \alternative {
  %   { \comp #8 }
  %   { \comp #8 }
  % }
  % \break
  % \mark "B"
  % \comp #32 \bar ".|"
  % \break
  % \mark "C"
  % \comp #32

}

Harmonies = \chords {
  \repeat unfold 1 {
    es4:m7 es:m7/d es2:m7/des | c2:dim as4:m9 des:7 | c1:m7.5- |
    b4:m7 e:7 bes:m7 es:7 | as2:m7 des:7 | es:m as:7.5- |
    c4:m7.5- b2.:7.5- | bes1:7.5-.9- | c4:m7.5- b:7.5- bes:7.4 es:6 | s1 |

    c2:m7.5- b:7.5- | bes1:7.5- | c2:m7.5- b:7.5- | bes1:7.5- | 
    as2:m7 f4:m7 bes:7 | c2:m7.5- f:7 | des:9 ces:9 | as2:m7 f4:m7 bes:7 |

    es4:m7 es:m7/d es2:m7/des | c2:dim as4:m9 des:7 | c1:m7.5- |
    b4:m7 e:7 bes:m7 es:7 | as2:m7 des:7 | es:m as:7.5- |
    c4:m7.5- b:7.5- bes:7.4 es:6 | s1 |
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { instrument = "C edition" }
  \score { \transpose c c, {
  << 
    \new ChordNames \Harmonies
    \new Staff = one {
      \relative c'' {
        \clef violin \key es \minor
        \melody
      }
    }
  >>
  } }
}

\bookpart {
  \header { instrument = "Eb edition" }

  \score { \transpose es c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key es \minor
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bb edition" }

  \score { \transpose bes c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c'' {
          \clef violin \key es \minor
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bass edition" }
  \score { \transpose c c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef bass \key es \minor
          \melody
        }
      }
    >>
  } }
}
% }}}
