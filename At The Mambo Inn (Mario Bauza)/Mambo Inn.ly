\version "2.16.2"

% {{{ Header
\header {
    title = "At The Mambo Inn"
    composer = \markup \center-column { "Mario Bauza, Grave Sampson" "and Bobby Woodlen" }
    meter = "Mambo, Latin"
    tagline = ""
}
% }}}

% {{{ Paper and layout settings
\paper {
    staff-height = 6.8\mm
    between-system-space = 7.3\cm
    between-system-padding = #1
    max-systems-per-page = 13
    left-margin = 2\cm
    right-margin = 2\cm
    top-margin = 1\cm
    bottom-margin = 2\cm
    indent  = 0.0\cm
    short-indent = 0\cm
}

% increase horizontal spacing
% \layout {
%   \context {
%     \Score
%     \override SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 16)
%   }
% }

% Function to print a specified number of slashes
comp = #(define-music-function (parser location count) (integer?)
#{
\override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
\override Rest #'thickness = #0.48
\override Rest #'slope = #1.7
\repeat unfold $count { r4 }
\revert Rest #'stencil
#}
)

% }}}

% {{{ Music

melody = {
  \mark "Intro"
  \new Voice <<
    { f'4 r8 c4  es  f8~ | f8 f4 c8  es  f r4 | }
  >>
  \new Voice <<
    { f4 r8 c4  es  f8~ | f8 f4 c8  es  f r4 | }
    { a4 r8 es4 g   a8~ | a8 a4 es8 g   a r4 | }
  >>
  \break
  \new Voice <<
    { f4 r8 c4  es  f8~ | f8 f4 c8  es  f r4 | }
    { a4 r8 es4 g   a8~ | a8 a4 es8 g   a r4 | }
    { c4 r8 g4  bes c8~ | c8 c4 g8  bes c r4 | }
    { f4 r8 c4  es  f8~ | f8 f4 c8  es  f r4 | }
  >>
  r1 | r1 |

  \break \mark "A"
  \repeat volta 2 {
    d,4 f g4. e8~ | e4. f8 r8 a4 d8 | c2 a4. c8~ | c2 r2 |
    d,4 f g4. e8~ | e4. f8 r8 a4 d8 | c2 a4. f8~ | f2 r2 |
  }

  \break \mark "B"
  f4. c'8~ c4 bes4 | c4. bes8~ bes4 c4 | es,4. bes'8~ bes4 as4 | bes2 r2 |
  g4. e'8~ e4 d4 | e4. d8~ d4 e4 | g,4. a8~ a4 g4 | a2 r2 |

  \break \mark "A"
  d,4 f g4. e8~ | e4. f8 r8 a4 d8 | c2 a4. c8~ | c2 r2 |
  d,4 f g4. e8~ | e4. f8 r8 a4 d8 | c2 a4. f8~ | f2 r2 |

  \break \mark "Solos"
  \repeat volta 2 { \comp #32 }

  \comp #64
}

Harmonies = \chords {
  r1*8
  \repeat unfold 2 {
    g2:m7 c:7 f:maj7 d:m7 g:m7 c:7 f1:maj7
    g2:m7 c:7 f:maj7 d:m7 g:m7 c:7 f1:6

    bes1:m7 es:7 as1*2:maj7
    d1:m7 g:7 g:m7 c:7

    g2:m7 c:7 f:maj7 d:m7 g:m7 c:7 f1:maj7
    g2:m7 c:7 f:maj7 d:m7 g:m7 c:7 f1:6
  }
}

% }}}

% {{{ Typesetting
\bookpart {
  \header { instrument = "C edition" }
  << 
    \new ChordNames \Harmonies
    \new Staff = one {
      \relative c {
        \clef violin \key f \major
        \melody
      }
    }
  >>

}

\bookpart {
  \header { instrument = "Eb edition" }

  \score { \transpose es c {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef violin \key f \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bb edition" }

  \score { \transpose bes c' {
    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c {
          \clef violin \key f \major
          \melody
        }
      }
    >>
  } }
}

\bookpart {
  \header { instrument = "Bass edition" }

    << 
      \new ChordNames \Harmonies
      \new Staff = one {
        \relative c,, {
          \clef bass \key f \major
          \melody
        }
      }
    >>
}
% }}}
